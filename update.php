<?php

include 'koneksi.php';

session_start();

if (isset($_GET["pesan"])){
    $pesan = $_GET["pesan"];
    } else {
    $pesan = " ";
    }


if ( !isset($_SESSION["login"]))
{
   header("location : index.php");
}

$id = $_GET["id"];

$sql = "SELECT * FROM users WHERE id='$id'";
$result = $koneksi -> query($sql);
$hasil = $result -> fetch_assoc();	
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>registrasi</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
</head>
<style>
  .body{
    background-image:url('background.jpg');
    background-repeat:no-repeat;
    background-size:1500px 900px;
  }
  .container{
    margin-top:100px;
  }
</style>
<body class="body">
<div class="container">
<div class="row justify-content-center">
<div class="col-12 col-sm-6 col-md-3">

  <div class="card">
    <div class="card-body">
	<nav class="navbar navbar-expand-lg navbar-dark bg-info">
        <a class="navbar-brand" href="https://bit.ly/346Z1qB" target="_blank"><i class="fas fa-graduation-cap"></i></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item-Light">
                        <h4 style="color:white">Ubah Data<span class="sr-only"></span></h4>
                    </li>
                </ul>
            </div>
    </nav>
    
    <?php
    if (isset($_GET["pesan"]))
    {
    $pesan = $_GET["pesan"]
    ?>
    <div class="alert alert-success" role="alert" style="margin-top:5px;"><?php  echo $pesan; ?></div>
    <?php
    } 
    ?>

    <form action="proses_update.php" method="POST" >

		    <input type="hidden" name="id" value="<?= $id?>">
        <div class="form-group col-md-4" style="margin-top:15px;width:200px">

		    <label>Nama</label>
			<input value="<?= $hasil["nama"] ?>" type="text" name="nama" required>
		</div>

		<div class="form-group col-md-4" style="margin-top:15px;width:200px">
            <label>Username</label>
			<input value="<?= $hasil["username"] ?>" type="text" name="username" required>
		</div>

        <div class="form-group col-md-4" style="margin-top:15px;width:200px"> 
	        <label>email</label>
			<input value="<?= $hasil["email"] ?>" type="email" name="email" required>
		</div>


	    <button class="btn btn-success" style="margin:12px">Ubah</button>

		<a class="btn btn-danger" href="user.php">kembali</a>

    </form>

        </div>
      </div>
</div>
</div>
</div>
</body>
</html>


</body>
</html>