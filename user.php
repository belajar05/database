<?php
include 'koneksi.php';
session_start();

$username = $_SESSION['username'];
$sql = "SELECT * FROM users";
$result = mysqli_query($koneksi, $sql);

//mysqli_fetch_all
$hasil = mysqli_fetch_all($result, MYSQLI_ASSOC);
?>


<!DOCTYPE html>
<html>
<head>
	<title>User</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
     <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<style>
	.title h5{
		border-radius:15px;
		margin:15px;
		padding:5px;
        text-align:center;
        font-family:georgia;
        background-color: #6c757d;
        color: white;
	}
	.alert{
		border-radius:10px;
		text-align:center;
		color:white;
        margin:10px;
        margin-right-left:40px;
        background-color:#28a745;
	}
</style>
<body>

	<nav class="navbar navbar-expand-lg navbar-dark bg-info">
        <a class="navbar-brand" href="https://bit.ly/346Z1qB" target="_blank"><i class="fas fa-graduation-cap"></i></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a href="home.php" class="nav-link" >Home <span class="sr-only">(current)</span></a>
      </li>
  </ul>
</div>
</nav>
<div class="title"><h5>Data User</h5></div>
<hr>
<?php
    if (isset($_GET["pesan"]))
    {
    $pesan = $_GET["pesan"]
    ?>
    <div class="alert" role="alert"><?php  echo $pesan; ?></div>
    <?php
    } 
    ?>
<a class="btn btn-primary" style="margin:15px;" href="tambah.php">Tambah</a>

    

	<div class="container">
    <table class="table table-hover">
    	<thead>
    		<tr>
    			<th>No.</th>
    			<th>Nama</th>
    			<th>username</th>
    			<th>Email</th>
    			<th>Avatar</th>
    			<th>Action</th>
    		</tr>
    	</thead>

    	<tbody>
    		<?php
    		foreach ($hasil as $key => $user_data) {
    			?>
    			<tr>
    				<td><?= $key + 1 ?></td>
    				<td><?= $user_data["nama"] ?></td>
    				<td><?= $user_data["username"] ?></td>
    				<td><?= $user_data["email"] ?></td>
    				<td><?= $user_data["avatar"] ?></td>
    				<td><a class="btn btn-success" href="update.php?id=<?= $user_data['id']?>">Ubah</a> <a class="btn btn-danger"  href="proses_hapus.php?id=<?= $user_data['id']?>">hapus</a></td>
    			</tr>
    			<?php
    		}
    		?>
    	</tbody>
    	<tfoot>
    		
    	</tfoot>
    </table>



    </div>
</body>
</html>