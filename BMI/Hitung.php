<?php
$nama = $_POST["nama"];
$kelamin = $_POST["kelamin"];
$tinggi = $_POST["tinggi"];
$berat = $_POST["berat"];
?>

<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
    <link rel="stylesheet" href="index.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-info">
  <a class="navbar-brand" href="https://bit.ly/346Z1qB"><i class="fas fa-graduation-cap"></i></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item-Light">
        <a class="nav-link" href="index.php">Kembali<span class="sr-only">(current)</span></a>
      </li>
    </ul>
  </div>
</nav>

<div class="nama">
<h4><?= $nama; ?></h4>
</div>
<div class="kelamin">
<h4>
<?php
if ($kelamin == "Laki-laki")
{
	echo "jenis Kelamin: Laki-laki";
}
elseif ($kelamin == "Perempuan")
{
echo "jenis Kelamin: Perempuan";
}
?>
</h4>
</div>
<div class="hasil">
<h4>
<?php
$tinggi = $tinggi/100;
$BMI = $berat/pow($tinggi,2);

if ($BMI <= "18.5")
{
	echo "Keterangan: Kekurangan Berat Badan";
}
elseif ($BMI >= "18.5" && $BMI <= "24.9")
{
echo "Keterangan: Normal/Ideal";
}
elseif ($BMI >= "24.9" && $BMI <= "29.9")
{
   echo "Keterangan: Kelebihan berat badan";
}
elseif ($BMI >= "30.0")
{
	echo "keterangan: kegemukan/obesitas";
}
?>
</h4>
</div>
</body>
</html>

