<?php
session_start();

if ( !isset($_SESSION["login"]))
{
  header("location: index.php");exit;
}
?>
<html lang="eng">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
     <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>LearnWebsite</title>

    <style type="text/css">
        .copy{
          color:white;
          padding-top:10px;
          padding-bottom:10px;
          margin:0px;
        }
        
      </style>

  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-info">
    <a class="navbar-brand" href="https://bit.ly/346Z1qB" target="_blank"><i class="fas fa-graduation-cap"></i></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a href="logout.php" class="nav-link" >Logout <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a href="BMI/index.php" class="nav-link" >BMI <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a href="user.php" class="nav-link" >User <span class="sr-only">(current)</span></a>
      </li>
      </ul>
  </div>
</nav>
<!--ini isi-->
<div class="card mb-3">
  <img src="cardhome.jpg" height="440px" class="card-img-top"/>
  <div class="card-body">
    <h5 class="card-title">Home</h5>
      <p class="card-text">selamat datang di halaman website saya</p>
      <br>
      <p class="card-text"><small class="text-muted">Last update: <?php echo date("d-l-M-y H:i:s", time() + (60*60*6));?></small></p>

  </div>
</div>

<!--INI FOOTER-->
<div class="footer footer-light bg-info">
      <h6 class="copy" align="center">Copyright 2021.Nur A'li.</h6>
</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.5.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>